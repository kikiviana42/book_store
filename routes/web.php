<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');

Route::group(['middleware' => ['auth']], function () {
    //
    // Route::get('/master', function () {
    //     return view('layout.master');
        
    // });
    
    //CRUD Kategori
    Route::get('/kategori/create', 'KategoriController@create');
    Route::post('/kategori', 'KategoriController@store');
    Route::get('/kategori', 'KategoriController@index');
    Route::get('/kategori/{kategori_id}', 'KategoriController@show');
    Route::get('/kategori/{kategori_id}/edit', 'KategoriController@edit');
    Route::put('/kategori/{kategori_id}', 'KategoriController@update');
    Route::delete('/kategori/{kategori_id}', 'KategoriController@destroy');
    
    //CRUD User
    Route::get('/user/create', 'UserController@create');
    Route::post('/user', 'UserController@store');
    Route::get('/user', 'UserController@index');
    Route::get('/user/{user_id}', 'UserController@show');
    Route::get('/user/{user_id}/edit', 'UserController@edit');
    Route::put('/user/{user_id}', 'UserController@update');
    Route::delete('/user/{user_id}', 'UserController@destroy');
    
    //CRUD Profil
    Route::get('/profile', 'ProfileController@index');
    Route::get('/profile/{profile_id}/edit', 'ProfileController@edit');
    Route::put('/profile/{profile_id}', 'ProfileController@update');

    Route::resource('profile', 'ProfileController')->only(['index', 'update', 'store']);
    Auth::routes();

});
//CRUD Buku
Route::get('/buku/create', 'BukuController@create');
Route::post('/buku', 'BukuController@store');
Route::get('/buku', 'BukuController@index');
Route::get('/buku/{buku_id}', 'BukuController@show');
Route::get('/buku/{buku_id}/edit', 'BukuController@edit');
Route::put('/buku/{buku_id}', 'BukuController@update');
Route::delete('/buku/{buku_id}', 'BukuController@destroy');

//CRUD Review
Route::get('/review/create', 'ReviewController@create');
Route::post('/review', 'ReviewController@store');
Route::get('/review', 'ReviewController@index');
Route::get('/review/{review}', 'ReviewController@show');
Route::get('/review/{review_id}/edit', 'ReviewController@edit');
Route::put('/review/{review_id}', 'ReviewController@update');
Route::delete('/review/{review_id}', 'ReviewController@destroy');




Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
