<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Buku extends Model
{
    protected $table="buku";
    protected $fillable=["judul","tahun","gambar","harga","penulis","penerbit","kategori_id"];

    public $timestamps = false;
    
    public function kategori()
    {
        return $this->belongsTo('App\Kategori');
    }

    public function review()
    {
        return $this->hasMany('App\Review');
    }
}
