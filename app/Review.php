<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    protected $table="review";
    protected $fillable=["review","rating","user_id","buku_id"];

    public $timestamps = false;
    
    public function buku(){
        return $this->belongsTo('App\Buku','buku_id');
    }

    public function user(){
        return $this->belongsTo('App\User','user_id');
    }
}
