<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\user;

class UserController extends Controller
{
    
    public function index(){
        $listuser = user::all();
        return view('user.index', compact('listuser'));
    }
    
    // public function create(){
    //     return view('user.create');
    // }

    // public function store(Request $request){
    //     $request->validate([
    //         'name' => 'required|unique:users',
    //         'email' => 'required',
    //         'password' => 'required'
    //     ]);
    //     $query = DB::table('users')->insert([
    //         "name" => $request["name"],
    //         "email" => $request["email"],
    //         "password" => $request["password"]
    //     ]);
    //     return redirect('/user');
    // }

    // public function index(){
    //     $listuser = DB::table('users')->get();
    //     // return view('user.index', compact('listuser'));
    // }

    // public function show($id){
    //     $user = DB::table('users')->where('id', $id)->first();
    //     return view('user.show', compact('user'));
    // }

    // public function edit($id){
    //     $user = DB::table('users')->where('id', $id)->first();
    //     return view('user.edit', compact('user'));
    // }

    // public function update(Request $request, $id){
    //     $request->validate([
    //         'name' => 'required|unique:users',
    //         'email' => 'required',
    //         'password' => 'required'
    //     ]);
    //     $query = DB::table('users')->where('id', $id)
    //     ->update([
    //         'name' => $request["name"],
    //         "email" => $request["email"],
    //         "password" => $request["password"]
    //     ]);
    //     return redirect('/user');
    // }

    // public function destroy($id){
    //     $query = DB::table('users')->where('id', $id)->delete();
    //     return redirect('/user');
    // }
}
