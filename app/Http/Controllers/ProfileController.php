<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Profile;
use App\User;
use Auth;

class ProfileController extends Controller
{
    // public function __construct()
    // {
    //     $this->middleware('auth')->except('index','update','store');
    // }
    
    public function index(){
        $profile = Profile::where('user_id', Auth::user()->id)->first();
        return view('profile.index', compact('profile'));
    }

    // public function show($id){
    //     $profile = DB::table('profile')->where('id', $id)->first();
    //     return view('profile.show', compact('profile'));
    // }

    public function edit($id){
        $profile = DB::table('profile')->where('id', $id)->first();
        return view('profile.edit', compact('profile'));
    }

    public function store(Request $request){
        $this->validate($request,[
            'alamat' => 'required',
            'umur' => 'required',
        ]);

        Profile::create([
            'alamat' => $request -> alamat,
            'umur' => $request -> umur,
            'user_id' => Auth::user()->id
        ]);
        return redirect('/profile');
    }

    public function update(Request $request, $id){
        $this->validate($request,[
            'alamat' => 'required',
            'umur' => 'required',
        ]);

        $profil_data =[
            'alamat' => $request->alamat,
            'umur' => $request->umur,
        ];
        Profile::whereId($id)->update($profil_data);
        return redirect('/profile');
    }

    // public function destroy($id){
    //     $query = DB::table('profile')->where('id', $id)->delete();
    //     return redirect('/profile');
    // }
}
