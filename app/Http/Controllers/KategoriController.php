<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;

use DB;
use App\Kategori;
 
class KategoriController extends Controller
{
    public function create(){
        return view('kategori.create');
    }

    public function store(Request $request){
        $request->validate([
            'nama' => 'required|unique:kategori',
        ]);
        $query = DB::table('kategori')->insert([
            "nama" => $request["nama"],
        ]);
        Alert::success('Berhasil', 'Berhasil Menambahkan Kategori');
        return redirect('/kategori');
    }

    public function index(){
        $listkategori=kategori::all();
        // $listkategori = DB::table('kategori')->get();
        return view('kategori.index', compact('listkategori'));
    }

    public function show($id){
        $kategori = DB::table('kategori')->where('id', $id)->first();
        return view('kategori.show', compact('kategori'));
    }

    public function edit($id){
        $kategori = DB::table('kategori')->where('id', $id)->first();
        return view('kategori.edit', compact('kategori'));
    }

    public function update(Request $request, $id){
        $request->validate([
            'nama' => 'required|unique:kategori',
        ]);
        $query = DB::table('kategori')->where('id', $id)
        ->update([
            'nama' => $request["nama"],
        ]);
        return redirect('/kategori');
    }

    public function destroy($id){
        $query = DB::table('kategori')->where('id', $id)->delete();
        return redirect('/kategori');
    }
}
