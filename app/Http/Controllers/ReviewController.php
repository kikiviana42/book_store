<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;

use DB;
use App\Review;
use App\Buku;
use App\User;
use Auth;

class ReviewController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except('index', 'show');
    }
    
    public function create(){
        $listbuku=buku::all();
        return view('review.create', compact('listbuku'));
    }

    public function store(Request $request){
        $request->validate([
            'review' => 'required',
            'rating' => 'required',
            'buku_id' => 'required',
            'user_id' => 'required'
        ]);

        $review = review::create([
            'review' => $request->review,
            'rating' => $request->rating,
            'buku_id' => $request->buku_id,
            'user_id' => Auth::user()->id,
        ]);
        Alert::info('Review', 'Tambah Review');
        return redirect('/review');
    }

    public function index(){
        $listreview = review::all();
        return view('review.index', compact('listreview'));
    }

    public function edit($id){
        $review = review::findorfail($id);
        $listbuku=buku::all();
        return view('review.edit', compact('listbuku','review'));
    }

    public function update(Request $request, $id){
        
        $request->validate([
            'review' => 'required',
            'rating' => 'required',
            'buku_id' => 'required',
            'user_id' => 'required'
        ]);

        $review = review::findorfail($id);
        
        $query = DB::table('review')->where('id', $id)
        ->update([
                'review' => $request->review,
                'rating' => $request->rating,
                'buku_id' => $request->buku_id,
                'user_id' => $request->user_id
        ]);
        return redirect('/review');
    }

    public function destroy($id){
        $query = DB::table('review')->where('id', $id)->delete();
        return redirect('/review');
    }
}
