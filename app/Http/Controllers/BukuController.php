<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;
use DB;
use App\Buku;
use App\Kategori;
use App\Review;
use File;


class BukuController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except('index', 'show');
    }
    
    public function create(){
        $listkategori=kategori::all();
        return view('buku.create', compact('listkategori'));
    }

    public function store(Request $request){
        $request->validate([
            'judul' => 'required',
            'tahun' => 'required',
            'gambar' => 'required|mimes: jpeg,jpg,png|max:2200',
            'harga' => 'required',
            'penulis' => 'required',
            'penerbit' => 'required',
            'kategori_id' => 'required'
        ]);

        $gambar = $request->gambar;
        $new_gambar=time().' - '.$gambar->getClientOriginalName();

        $buku = buku::create([
            'judul' => $request->judul,
            'tahun' => $request->tahun,
            'gambar' => $new_gambar,
            'harga' => $request->harga,
            'penulis' => $request->penulis,
            'penerbit' => $request->penerbit,
            'kategori_id' => $request->kategori_id
        ]);
        
        $gambar->move('uploads/buku/', $new_gambar);
        Alert::success('Berhasil', 'Tambah Buku Berhasil');
        return redirect('/buku');
    }

    public function index(){
        $listbuku = buku::all();
        return view('buku.index', compact('listbuku'));
    }

    public function show($id){
        $buku = DB::table('buku')->where('id', $id)->first();
        return view('buku.show', compact('buku'));
    }

    public function edit($id){
        $buku = buku::findorfail($id);
        $listkategori=kategori::all();
        return view('buku.edit', compact('listkategori','buku'));
    }

    public function update(Request $request, $id){
        
        $this->validate($request, [
            'judul' => 'required',
            'tahun' => 'required',
            'gambar' => 'mimes: jpeg,jpg,png|max:2200',
            'harga' => 'required',
            'penulis' => 'required',
            'penerbit' => 'required',
            'kategori_id' => 'required'
        ]);

        $buku = Buku::findorfail($id);
        
        if($request->has('gambar')){
            $path = 'uploads/buku/';
            File::delete($path , $buku->gambar);
            $gambar = $request->gambar;
            $new_gambar= time().' - '.$gambar->getClientOriginalName();
            $gambar->move('uploads/buku/', $new_gambar);
        
            $post_data = [
                'judul' => $request->judul,
                'tahun' => $request->tahun,
                'gambar' => $new_gambar,
                'harga' => $request->harga,
                'penulis' => $request->penulis,
                'penerbit' => $request->penerbit,
                'kategori_id' => $request->kategori_id
            ];
        } else {
            $post_data = [
                'judul' => $request->judul,
                'tahun' => $request->tahun,
                'harga' => $request->harga,
                'penulis' => $request->penulis,
                'penerbit' => $request->penerbit,
                'kategori_id' => $request->kategori_id
            ];
        }
        
        $buku->update($post_data);
        Alert::success('Berhasil', 'Berhasil Edit Buku');
        return redirect('/buku');
    }

    public function destroy($id){
        $query = DB::table('buku')->where('id', $id)->delete();
        return redirect('/buku');
    }
}
