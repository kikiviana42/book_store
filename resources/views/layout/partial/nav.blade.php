<nav class="navbar header-navbar pcoded-header">
    <div class="navbar-wrapper">
        <div class="navbar-logo">
            <a class="mobile-menu waves-effect waves-light" id="mobile-collapse" href="#!">
                <i class="ti-menu"></i>
            </a>
            <div class="mobile-search waves-effect waves-light">
                <div class="header-search">
                    <div class="main-search morphsearch-search">
                        <div class="input-group">
                            <span class="input-group-addon search-close"><i class="ti-close"></i></span>
                            <input type="text" class="form-control" placeholder="Enter Keyword">
                            <span class="input-group-addon search-btn"><i class="ti-search"></i></span>
                        </div>
                    </div>
                </div>
            </div>
            <a href="{{asset('mega-able-lite/index.html')}}">
                <img class="img-fluid" src="{{asset('mega-able-lite/assets/images/logo.png')}}" alt="Theme-Logo" />
            </a>
            <a class="mobile-options waves-effect waves-light">
                <i class="ti-more"></i>
            </a>
        </div>
      
        <div class="navbar-container container-fluid">
            <ul class="nav-left">
                <li>
                    <div class="sidebar_toggle"><a href="{{asset('mega-able-lite/javascript:void(0)')}}"><i class="ti-menu"></i></a></div>
                </li>
                <li class="header-search">
                    <div class="main-search morphsearch-search">
                        <div class="input-group">
                            <span class="input-group-addon search-close"><i class="ti-close"></i></span>
                            <input type="text" class="form-control">
                            <span class="input-group-addon search-btn"><i class="ti-search"></i></span>
                        </div>
                    </div>
                </li>
                <li>
                    <a href="#!" onclick="javascript:toggleFullScreen()" class="waves-effect waves-light">
                        <i class="ti-fullscreen"></i>
                    </a>
                </li>
            </ul>
            <ul class="nav-right">
                <li class="user-profile header-notification">
                    <a href="#!" class="waves-effect waves-light">
                        <img src="{{asset('mega-able-lite/assets/images/avatar-4.jpg')}}" class="img-radius" alt="User-Profile-Image">
                        @auth
                        <span id="more-details"> {{ Auth::user()->name }}</span>
                        @endauth
                        @guest
                        <span id="more-details"> Belum Login</span>
                        @endguest
                        <i class="ti-angle-down"></i>
                    </a>
                    <ul class="show-notification profile-notification">
                        <li class="waves-effect waves-light">
                            <a href="/profile">
                                <i class="ti-user"></i> Profile
                            </a>
                        </li>
                        @auth
                        <li>
                            {{-- <a id="navbarDropdown" class="nav-link" href="#" role="button">
                            </a> --}}

                                <a class="waves-effect waves-dark" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                    <span class="pcoded-micon"><i class="ti-layers"></i><b></b></span>
                                    <span class="pcoded-mtext" data-i18n="nav.form-components.main">Logout</span>
                                    <span class="pcoded-mcaret"></span>
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST">
                                    @csrf
                                </form>
                        </li>
                        @endauth
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</nav>
