@extends('layout.master')

@section('title')
    Edit Profil {{Auth::user()->name}}
@endsection

@section('content')

<!-- Form start -->
<form action="/profile/{{$profile->id}}" method="POST">
    @csrf
    @method('put')
    <div class="form-group">
        <label for="email">Email</label>
        <input type="text" class="form-control" name="email" value="{{Auth::user()->email}}" id="alamat" placeholder="Masukkan Alamat">
        @error('email')
            <div class="alert alert-danger mt-2">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="alamat">Alamat</label>
        <input type="text" class="form-control" name="alamat" value="{{Auth::user()->profile->alamat}}" id="alamat" placeholder="Masukkan Alamat">
        @error('alamat')
            <div class="alert alert-danger mt-2">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="umur">Umur</label>
        <input type="text" class="form-control" name="umur" value="{{Auth::user()->profile->umur}}" id="umur" placeholder="Masukkan Umur">
        @error('umur')
            <div class="alert alert-danger mt-2">
                {{ $message }}
            </div>
        @enderror
    </div> 
    <button type="submit" class="btn btn-primary">Update</button>
</form>
<!-- Form end -->

@endsection