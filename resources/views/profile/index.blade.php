@extends('layout.master')

@section('title')
Profil user {{Auth::user()->name}}
@endsection

@section('judul')
Profil user {{Auth::user()->name}}
@endsection

@section('content')

@if ($profile != null)
<h5>Nama : {{$profile->user->name}}</h5>
<h5>Email : {{$profile->user->email}}</h5>
<form action="/profile/{{$profile->id}}" method="POST" enctype="multipart/form-data">
    @csrf
    @method('put')
    <div class="form-group">
        <label for="alamat">Alamat</label>
        <textarea name="alamat" id="alamat" class="form-control">{{$profile->alamat}}</textarea>
        @error('alamat')
            <div class="alert alert-danger mt-2">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="umur">Umur</label>
        <input type="text" class="form-control" name="umur" value="{{$profile->umur}}" id="umur" placeholder="Masukkan Umur">
        @error('umur')
            <div class="alert alert-danger mt-2">
                {{ $message }}
            </div>
        @enderror
    </div>
    <a href="/profile/{{Auth::user()->id}}/edit" class="btn btn-primary">Edit</a>
</form>

    
@else
<form action="/profile" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="form-group">
        <label for="alamat">Alamat</label>
        <textarea name="alamat" id="alamat" class="form-control"></textarea>
        @error('alamat')
            <div class="alert alert-danger mt-2">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="umur">Umur</label>
        <input type="text" class="form-control" name="umur" id="umur" placeholder="Masukkan Umur">
        @error('umur')
            <div class="alert alert-danger mt-2">
                {{ $message }}
            </div>
        @enderror
    </div>
    <button type="submit" class="btn btn-primary">Tambah</button>
</form>

@endif

{{-- <!-- Content start -->
<p> Nama : {{Auth::user()->name}}</p>
<p>Email : {{Auth::user()->email}}</p>
<p>Alamat : {{Auth::user()->profile->alamat}}</p>
<p>Umur : {{Auth::user()->profile->umur}}</p>


<!-- Content end --> --}}
                              
@endsection