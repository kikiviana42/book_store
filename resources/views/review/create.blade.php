@extends('layout.master')

@section('title')
    Tambah Review
@endsection

@section('judul')
    Masukkan review
@endsection

@section('content')

<!-- Form start -->
<form action="/review" method="POST">
    @csrf
    <input type="hidden" class="form-control" name="user_id" value="1" id="user_id" placeholder="Masukkan user_id">
    <div class="form-group">
        <label for="review">Review</label>
        <input type="text" class="form-control" name="review" id="review" placeholder="Masukkan Review">
        @error('review')
            <div class="alert alert-danger mt-2">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="rating">Rating</label>
        <input type="text" class="form-control" name="rating" id="rating" placeholder="Masukkan rating">
        @error('rating')
            <div class="alert alert-danger mt-2">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="buku_id">Buku</label>
        <select class="form-control form-control-sm" name="buku_id" id="buku_id">
            <option>-- Pilih Buku --</option>
            @foreach ($listbuku as $item)
                <option value="{{$item->id}}">{{$item->judul}}</option>
            @endforeach
        </select>
        @error('buku')
            <div class="alert alert-danger mt-2">
                {{ $message }}
            </div>
        @enderror
    </div> 
    <button type="submit" class="btn btn-primary">Tambah</button>
</form>
<!-- Form end -->

@endsection