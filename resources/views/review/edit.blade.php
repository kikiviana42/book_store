@extends('layout.master')

@section('title')
    Edit Review
@endsection

@section('content')

<!-- Form start -->
<form action="/review/{{$review->id}}" method="POST">
    @csrf
    @method('put')
    <input type="hidden" class="form-control" name="user_id" value="{{$review->user_id}}" id="user_id" placeholder="Masukkan user_id">
    <div class="form-group">
        <label for="review">Review</label>
        <input type="text" class="form-control" name="review" value="{{$review->review}}" id="review" placeholder="Masukkan Review">
        @error('review')
            <div class="alert alert-danger mt-2">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="rating">Rating</label>
        <input type="text" class="form-control" name="rating" value="{{$review->rating}}" id="rating" placeholder="Masukkan rating">
        @error('rating')
            <div class="alert alert-danger mt-2">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="buku_id">Buku</label>
        <select class="form-control form-control-sm" name="buku_id" id="buku_id">
            <option>-- Pilih Buku --</option>
            @foreach ($listbuku as $item)
                @if ($item->id === $review->buku_id)
                    <option value="{{$item->id}}" selected>{{$item->judul}}</option>
                @else
                    <option value="{{$item->id}}">{{$item->judul}}</option>
                @endif
            @endforeach
        </select>
        @error('buku')
            <div class="alert alert-danger mt-2">
                {{ $message }}
            </div>
        @enderror
    </div> 
    <button type="submit" class="btn btn-primary">Update</button>
</form>
<!-- Form end -->

@endsection