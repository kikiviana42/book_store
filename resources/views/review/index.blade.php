@extends('layout.master')

@section('title')
    Review
@endsection

@section('judul')
    Review buku 
@endsection

@section('content')
{{-- <a href="/review/create" class="btn btn-primary mb-2">Tambah Review</a>
    <table class="table">
        <thead class="thead-light">
            <tr>
                <th scope="col">#</th>
                <th scope="col">Buku</th>
                <th scope="col">User</th>
                <th scope="col">Review</th>
                <th scope="col">Actions</th>
            </tr>
        </thead>
        <tbody>
            @forelse ($listreview as $key=>$value)
                <tr>
                    <td>{{$key + 1}}</td>
                    <td>{{$value->buku->judul}}</td>
                    <td>{{$value->user->name}}</td>
                    <td>{{$value->review}}</td>
                </tr>
                <td>
                    <form action="/review/{{$value->id}}" method="POST">
                        <a href="/review/{{$value->id}}/edit" class="btn btn-primary">Edit</a>
                        @csrf
                        @method('DELETE')
                        <input type="submit" class="btn btn-danger my-1 " value="Delete">
                    </form>
                </td>
            @empty
                <tr colspan="3">
                    <td> No Data</td>
                </tr>
            @endforelse
        </tbody>
    </table> --}}
<!-- Content start -->
<a href="/review/create" class="btn btn-primary mb-2">Tambah Review</a>

@foreach ($listreview as $item)
    <div class="card">
    <div class="card-header">
        <h5>Review buku {{$item->buku->judul}}</h5>
    </div>
    <div class="card-body">
        <p class="card-text"><h4><i>"{{$item->review}}"</i></h4></p>
        <p><h6>Rating : {{$item->rating}}</h6></p>
        <p><h6>User: {{$item->user->name}}</h6></p>
        @auth
            <a href="/review/{{$item->id}}/edit" class="btn btn-primary">Edit</a>
            <form action="/review/{{$item->id}}" method="post">
            @csrf
            @method('DELETE')
            <input type="submit" class="btn btn-danger my-1 " value="Delete">
        @endauth

        </form>
    </div>
    </div>
    
@endforeach
<!-- Content end -->
                              
@endsection