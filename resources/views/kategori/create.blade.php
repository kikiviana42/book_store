@extends('layout.master')

@section('title')
    Tambah Kategori
@endsection

@section('judul')
    Masukkan data kategori
@endsection

@section('content')

<!-- Form start -->
<form action="/kategori" method="POST">
    @csrf
    <div class="form-group">
        <label for="nama">Nama Kategori</label>
        <input type="text" class="form-control" name="nama" id="nama" placeholder="Masukkan Nama Kategori">
        @error('nama')
            <div class="alert alert-danger mt-2">
                {{ $message }}
            </div>
        @enderror
    </div> 
    <button type="submit" class="btn btn-primary">Tambah</button>
</form>
<!-- Form end -->

@endsection