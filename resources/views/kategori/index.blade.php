@extends('layout.master')

@section('title')
    Kategori
@endsection

@section('judul')
    Data Kategori
@endsection

@section('content')

<!-- Content start -->
<a href="/kategori/create" class="btn btn-primary mb-2">Tambah Kategori</a>
<table class="table">
    <thead class="thead-light">
        <tr>
        <th scope="col">#</th>
        <th scope="col">Nama</th>
        <th scope="col">List Buku</th>
        <th scope="col">Actions</th>
        </tr>
    </thead>
    <tbody>
        @forelse ($listkategori as $key=>$value)
            <tr>
                <td>{{$key + 1}}</th>
                <td>{{$value->nama}}</td>
                <td>
                    <ul>
                        @foreach ($value->buku as $buku)
                            <li>{{$buku->judul}}</li>
                        @endforeach
                    </ul>
                </td>
                <td>
                    
                    <form action="/kategori/{{$value->id}}" method="POST">
                        <a href="/kategori/{{$value->id}}" class="btn btn-info">Show</a>
                        <a href="/kategori/{{$value->id}}/edit" class="btn btn-primary">Edit</a>
                        @csrf
                        @method('DELETE')
                        <input type="submit" class="btn btn-danger my-1" value="Delete">
                    </form>
                </td>
            </tr>
        @empty
            <tr colspan="3">
                <td>No data</td>
            </tr>  
        @endforelse              
    </tbody>
</table>
<!-- Content end -->

@endsection