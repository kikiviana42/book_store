@extends('layout.master')

@section('title')
    Edit Kategori
@endsection

@section('judul')
    Edit Kategori {{$kategori->nama}}
@endsection

@section('content')

<!-- Form start -->
<form action="/kategori/{{$kategori->id}}" method="POST">
    @csrf
    @method('put')
    <div class="form-group">
        <label for="nama">Nama Kategori</label>
        <input type="text" class="form-control" name="nama" value="{{$kategori->nama}}" id="nama" placeholder="Masukkan Nama Kategori">
        @error('nama')
            <div class="alert alert-danger mt-2">
                {{ $message }}
            </div>
        @enderror
    </div>
    <button type="submit" class="btn btn-primary">Update</button>
</form>
<!-- Form end -->

@endsection