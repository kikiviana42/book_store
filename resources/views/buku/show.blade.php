@extends('layout.master')

@section('title')
    Detail Buku
@endsection

@section('judul')
    Detail buku {{$buku->judul}}
@endsection

@section('content')

<!-- Content start -->
<div class="row">
    <div class="col-4">
        <img src="{{asset('uploads/buku/'.$buku->gambar)}}" class="" alt="">
    </div>
    <div class="col-8">
        <h3>{{$buku->judul}} {{$buku->tahun}}</h3>
        <p>Harga : {{$buku->harga}}</p>
        <p>Penulis : {{$buku->penulis}}</p>
        <p>Penerbit : {{$buku->penerbit}}</p>
        {{-- <ul class="list-group list-group-flush">
            @foreach ($buku->review as $item)
                <li class="list-group-item"> {{$item->user->name}}-> {{$item->review}}</li>
            @endforeach
        </ul> --}}
        <a href="/buku" class="btn btn-primary btn-sm">Kembali</a>
        <a href="/review" class="btn btn-primary btn-sm">Review</a>
    </div>

</div>

<!-- Content end -->

@endsection