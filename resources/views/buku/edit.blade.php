@extends('layout.master')

@section('title')
    Edit Buku
@endsection

@section('judul')
    Edit data buku {{$buku->judul}}
@endsection

@section('content')

<!-- Form start -->
<form action="/buku/{{$buku->id}}" method="POST" enctype="multipart/form-data">
    @csrf
    @method('put')
    <div class="form-group">
        <label for="judul">Judul</label>
        <input type="text" class="form-control" name="judul" value="{{$buku->judul}}" id="judul" placeholder="Masukkan Judul">
        @error('judul')
            <div class="alert alert-danger mt-2">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="tahun">Tahun</label>
        <input type="text" class="form-control" name="tahun" value="{{$buku->tahun}}" id="tahun" placeholder="Masukkan tahun">
        @error('tahun')
            <div class="alert alert-danger mt-2">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="gambar">Gambar</label>
        <input type="file" class="form-control" name="gambar" id="gambar">
        @error('gambar')
            <div class="alert alert-danger mt-2">
                {{ $message }}
            </div>
        @enderror
    </div> 
    <div class="form-group">
        <label for="harga">Harga</label>
        <input type="text" class="form-control" name="harga" value="{{$buku->harga}}" id="harga" placeholder="Masukkan harga">
        @error('harga')
            <div class="alert alert-danger mt-2">
                {{ $message }}
            </div>
        @enderror
    </div> 
    <div class="form-group">
        <label for="penulis">Penulis</label>
        <input type="text" class="form-control" name="penulis" value="{{$buku->penulis}}" id="penulis" placeholder="Masukkan penulis">
        @error('penulis')
            <div class="alert alert-danger mt-2">
                {{ $message }}
            </div>
        @enderror
    </div> 
    <div class="form-group">
        <label for="penerbit">Penerbit</label>
        <input type="text" class="form-control" name="penerbit" value="{{$buku->penerbit}}" id="penerbit" placeholder="Masukkan penerbit">
        @error('penerbit')
            <div class="alert alert-danger mt-2">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="kategori_id">ID Kategori</label>
        <select class="form-control form-control-sm" name="kategori_id" id="kategori_id">
            <option>-- Pilih Kategori --</option>
            @foreach ($listkategori as $item)
                @if ($item->id === $buku->kategori_id)
                    <option value="{{$item->id}}" selected>{{$item->nama}}</option>
                @else
                    <option value="{{$item->id}}">{{$item->nama}}</option>
                @endif
            @endforeach
        </select>
    </div>
    <button type="submit" class="btn btn-primary">Update</button>
</form>
<!-- Form end -->

@endsection