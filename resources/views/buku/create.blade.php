@extends('layout.master')

@section('title')
    Tambah Buku
@endsection

@section('judul')
    Masukkan data buku
@endsection

@section('content')

<!-- Form start -->
<form action="/buku" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="form-group">
        <label for="judul">Judul</label>
        <input type="text" class="form-control" name="judul" id="judul" placeholder="Masukkan Judul">
        @error('judul')
            <div class="alert alert-danger mt-2">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="tahun">Tahun</label>
        <input type="text" class="form-control" name="tahun" id="tahun" placeholder="Masukkan tahun">
        @error('tahun')
            <div class="alert alert-danger mt-2">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="gambar">Gambar</label>
        <input type="file" class="form-control" name="gambar" id="gambar">
        @error('gambar')
            <div class="alert alert-danger mt-2">
                {{ $message }}
            </div>
        @enderror
    </div> 
    <div class="form-group">
        <label for="harga">Harga</label>
        <input type="text" class="form-control" name="harga" id="harga" placeholder="Masukkan harga">
        @error('harga')
            <div class="alert alert-danger mt-2">
                {{ $message }}
            </div>
        @enderror
    </div> 
    <div class="form-group">
        <label for="penulis">Penulis</label>
        <input type="text" class="form-control" name="penulis" id="penulis" placeholder="Masukkan penulis">
        @error('penulis')
            <div class="alert alert-danger mt-2">
                {{ $message }}
            </div>
        @enderror
    </div> 
    <div class="form-group">
        <label for="penerbit">Penerbit</label>
        <input type="text" class="form-control" name="penerbit" id="penerbit" placeholder="Masukkan penerbit">
        @error('penerbit')
            <div class="alert alert-danger mt-2">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="kategori_id">ID Kategori</label>
        <select class="form-control form-control-sm" name="kategori_id" id="kategori_id">
            <option>-- Pilih Kategori --</option>
            @foreach ($listkategori as $item)
                <option value="{{$item->id}}">{{$item->nama}}</option>
            @endforeach
        </select>
    </div>
    <button type="submit" class="btn btn-primary">Tambah</button>
</form>
<!-- Form end -->

@endsection