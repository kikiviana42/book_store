@extends('layout.master')

@section('title')
    List Buku
@endsection

@section('judul')
    Data buku
@endsection

@section('content')

@auth
<a href="/buku/create" class="btn btn-primary mb-2">Tambah Buku</a>
@endauth
<!-- Content start -->

<br>
<div class="row">
    @foreach ($listbuku as $item)
        <div class="col-4">
            <div class="card" >
            <img class="card-img-top" src="{{asset('uploads/buku/'.$item->gambar)}}" alt="Card image cap">
            <div class="card-body">
                <h4>{{$item->judul}} ({{$item->tahun}})</h4>
                <p class="card-text">
                <span class="badge badge-primary">{{$item->kategori->nama}}</span>
                   <p>Penulis : {{$item->penulis}}</p> 
                    <p>Penerbit: {{$item->penerbit}}</p> 
                    <p><h5>Rp {{$item->harga}},-</h5> </p>
                </p>
                <form action="/buku/{{$item->id}}" method="POST">
                    <a href="/buku/{{$item->id}}" class="btn btn-info btn-sm">Show</a>
                    @auth
                    <a href="/buku/{{$item->id}}/edit" class="btn btn-primary btn-sm">Edit</a>
                    @csrf
                    @method('DELETE')
                    <input type="submit" class="btn btn-danger my-1 btn-sm" value="Delete"> 
                    @endauth

                </form>
            </div>
            </div>
        </div>
@endforeach
</div>


<!-- Content end -->

@endsection